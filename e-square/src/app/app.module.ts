import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { SearchComponent } from './components/search/search.component';
import { WishlistComponent } from './components/wishlist/wishlist.component';
import { StoreModule } from '@ngrx/store';

import * as fromApp from './store/app.reducer';
import { EffectsModule } from '@ngrx/effects';
import { SearchEffects } from './components/search/store/search.effects';

import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {StoreRouterConnectingModule} from '@ngrx/router-store';

import { environment } from 'src/environments/environment';
import { DialogBoxComponent } from './components/dialog-box/dialog-box.component';
import { NavbarComponent } from './components/navbar/navbar.component';

import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    SearchComponent,
    WishlistComponent,
    DialogBoxComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(fromApp.appReducer),
    EffectsModule.forRoot([SearchEffects]),
    StoreDevtoolsModule.instrument({logOnly : environment.production}),
    StoreRouterConnectingModule.forRoot(),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
