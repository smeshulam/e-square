export interface ApiResponse {
  kind: string;
  totalItems: number;
  items: Volume[];
}

interface VolumeInfo {
  title: string;
  subtitle: string;
  authors: string[];
  publisher: string,
  publishDate: number,
  industryIdentifiers: industryIdentifier[],
  readingModes: ReadingModes,
  pageCount: number,
  printType: string,
  categories: string[],
  maturityRating: string,
  allowAnonLogging: boolean,
  contentVersion: string,
  panelizationSummary: PanelizationSummary,
  language: string,
  previewLink: string,
  infoLink: string,
  canonicalVolumeLink: string
}

interface industryIdentifier {
  type: string;
  identifier: string;
  description: string,
}

interface ReadingModes {
  text: boolean;
  image: boolean;
}

interface PanelizationSummary {
  containsEpubBubbles: boolean,
  containsImageBubbles: boolean
}

interface ImageLinks {
  smallThumbnail: string,
  thumbnail: string
}

export class Volume {
  constructor(
    public kind: string,
    public id: string,
    public etag: string,
    public selfLink: string,
    public volumeInfo: VolumeInfo,
  ) { }
}