import { ActionReducerMap } from '@ngrx/store';

import * as fromSearch from '../components/search/store/search.reducer';

import * as fromWishlist from '../components/wishlist/store/wishlist.reducer';

import * as fromWelcome from '../components/welcome/store/welcome.reducer';


export interface AppState {
  search: fromSearch.State;
  wishlist: fromWishlist.State;
  welcome: fromWelcome.State;
}

export const appReducer: ActionReducerMap<AppState> = {
  search: fromSearch.searchReducer,
  wishlist: fromWishlist.wishlistReducer,
  welcome: fromWelcome.welcomeReducer
}