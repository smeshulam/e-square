import { createAction, props } from '@ngrx/store';
import { Volume } from 'src/app/models/apiModels';

export const addToWishlist = createAction(
  '[Wishlist] Add To Wishlist',
  props<{ wish: Volume }>()
) 

export const removeItemFromWishlist = createAction(
  '[Wishlist] Remove Item From Wishlist',
  props<{ index: number }>()
)