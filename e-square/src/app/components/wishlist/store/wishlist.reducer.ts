import { Action, createReducer, on } from '@ngrx/store';

import * as WishlistActions from './wishlist.actions';
import { Volume } from 'src/app/models/apiModels';

export interface State {
  wishlist: Volume[]
}

const initialState: State = {
  wishlist: []
}

export function wishlistReducer(wishlistState: State | undefined, wishlistAction: Action) {
  return createReducer(
    initialState,
    on(WishlistActions.addToWishlist,
      (state, action) => ({
        ...state,
        wishlist: state.wishlist.concat(action.wish)
      })
    ),
    on(WishlistActions.removeItemFromWishlist,
      (state, action) => ({
        ...state,
        wishlist: state.wishlist.filter((wish, index) => index !== action.index)
      })
    )
  )(wishlistState, wishlistAction);
}