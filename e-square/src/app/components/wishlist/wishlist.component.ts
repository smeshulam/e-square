import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducer';
import { Volume } from 'src/app/models/apiModels';
import { Observable } from 'rxjs';

import * as WishlistActions from './store/wishlist.actions';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {
  wishlist: Observable<{wishlist: Volume[]}>;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.wishlist = this.store.select('wishlist');
  }

  onItemRemove(index: number) {
    this.store.dispatch(WishlistActions.removeItemFromWishlist({index}));
  }

}
