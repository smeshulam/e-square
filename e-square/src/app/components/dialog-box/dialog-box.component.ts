import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { Volume } from 'src/app/models/apiModels';
import * as fromApp from '../../store/app.reducer';
import * as WishlistActions from '../wishlist/store/wishlist.actions';

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.css']
})
export class DialogBoxComponent {
  constructor(private store: Store<fromApp.AppState>) {}

  @Input() book: Volume;
  @Output() close: EventEmitter<void> = new EventEmitter();

  onClose() {
    this.close.emit();
  }

  onAddToWishList() {
    this.store.dispatch(WishlistActions.addToWishlist({ wish: this.book }));
    this.onClose();
  }

}

