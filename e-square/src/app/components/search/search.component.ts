import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducer';

import * as SearchActions from './store/search.actions';

import * as WishlistActions from '../wishlist/store/wishlist.actions';

import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Volume } from 'src/app/models/apiModels';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  searchResults: Volume[];
  query: string;
  numPages: number;
  currentPage: number;
  selectedItem: Volume;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.subscription = this.store.select('search')
      .subscribe(searchState => {
        this.searchResults = searchState.volumes;
        this.numPages = searchState.numPages;
        this.currentPage = searchState.currentPage;
        this.selectedItem = searchState.selectedItem;
        this.query = searchState.query;
      })
  }

  onInput(e: Event) {
    const query = (<HTMLInputElement>e.target).value;
    this.store.dispatch(SearchActions.search({query}))
  }

  array(n: number): any[] {
    return Array(n);
  }

  onPaginate(numPage: number) {
    this.store.dispatch(SearchActions.paginate({numPage}));
  }

  onSelectItem(volume: Volume) {
    this.selectedItem = volume;
  }

  onDialogClose() {
    this.selectedItem = null;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
