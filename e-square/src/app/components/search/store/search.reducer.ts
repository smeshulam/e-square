import { Action, createReducer, on } from '@ngrx/store';

import * as SearchActions from './search.actions';

import { Volume } from 'src/app/models/apiModels';

export interface State {
  selectedItem: Volume,
  startIndex: number,
  currentPage: number,
  numPages: number,
  query: string,
  volumes: Volume[]
}

const initialState: State = {
  selectedItem: null,
  startIndex: 0,
  currentPage: 0,
  numPages: 0,
  query: '',
  volumes: null
}

export function searchReducer(searchState: State | undefined, searchAction: Action) {
  return createReducer(
    initialState,
    on(SearchActions.setResults,
      (state, action) => ({
        ...state,
        numPages: action.numPages,
        volumes: action.results
      })
    ),
    on(SearchActions.search,
      (state, action) => ({
        ...state,
        currentPage: 0,
        numPages: 0,
        startIndex: 0,
        query: action.query
      })
    ),
    on(SearchActions.paginate,
      (state, action) => ({
        ...state,
        currentPage: action.numPage,
        startIndex: action.numPage * 20
      })
    ),
  )(searchState, searchAction);
}