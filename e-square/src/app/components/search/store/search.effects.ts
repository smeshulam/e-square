import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Actions, ofType, createEffect } from '@ngrx/effects';

import * as SearchActions from './search.actions';

import * as fromApp from '../../../store/app.reducer';

import { ApiResponse } from '../../../models/apiModels';

import { switchMap, map, withLatestFrom } from 'rxjs/operators';
import { Store } from '@ngrx/store';

@Injectable()
export class SearchEffects {
  private apiKey: string = 'AIzaSyCr1ymVd2yuvlxHu8v3FNGji4U-DMbWbrA';
  private apiUrl: string = 'https://www.googleapis.com/books/v1/volumes';

  search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SearchActions.search, SearchActions.paginate),
      withLatestFrom(this.store.select('search')),
      switchMap(([actionData, searchState]) => {
        let searchParams = new HttpParams();
        searchParams = searchParams.append('q', searchState.query);
        searchParams = searchParams.append('key', this.apiKey);
        searchParams = searchParams.append('startIndex', searchState.startIndex.toString());
        searchParams = searchParams.append('maxResults', '20');
        return this.http.get<ApiResponse>(this.apiUrl, { params: searchParams });
      }),
      map(apiRes => {
        return SearchActions.setResults({ results: apiRes.items, numPages: Math.ceil(apiRes.totalItems / 20) })
      })
    )
  )

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private store: Store<fromApp.AppState>
  ) { }
}