import { createAction, props } from '@ngrx/store';
import { Volume } from 'src/app/models/apiModels';

export const setResults = createAction(
  '[Search] Set Results',
  props<{ results: Volume[], numPages: number }>()
);
export const search = createAction(
  '[Search] Search',
  props<{ query: string }>()
);
export const paginate = createAction(
  '[Search] Paginate',
  props<{ numPage: number }>()
)