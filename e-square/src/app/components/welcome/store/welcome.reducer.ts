import { Action, createReducer, on } from '@ngrx/store';

import * as WelcomeActions from './welcome.actions';

export interface State {
  name: string
}

const initialState: State = {
  name: null
}

export function welcomeReducer(welcomeState: State | undefined, welcomeAction: Action) {
  return createReducer(
    initialState,
    on(WelcomeActions.setName,
      (state, action) => ({
        ...state,
        name: action.name
      })
    )
  )(welcomeState, welcomeAction);

}