import { createAction, props } from '@ngrx/store';

export const setName = createAction(
  '[Welcome] Set Name',
  props<{ name: string }>()
)