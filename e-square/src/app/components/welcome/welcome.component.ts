import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import * as fromApp from '../../store/app.reducer';
import * as WelcomeActions from './store/welcome.actions';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(private store: Store<fromApp.AppState>, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(f: NgForm) {
    this.store.dispatch(WelcomeActions.setName(f.value));
    this.router.navigate(['/search']);
  }

}
