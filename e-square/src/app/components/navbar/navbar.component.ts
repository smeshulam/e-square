import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromApp from '../../store/app.reducer';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  name:  Observable<{name: String}>;

  constructor(private store:Store<fromApp.AppState>) { }

  ngOnInit() {
    this.name = this.store.select('welcome')
  }

}
